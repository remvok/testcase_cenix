<?php

namespace App\Http\Controllers;

use App\Application;
use Illuminate\Http\Request;

class ApplicationController extends Controller
{
    public function index(Request $request)
    {
        $email = $request->cookie('email');
        $application = (isset($email) ? Application::where('email', $email)->first() : []);
        return view('applications.index', [
            'application' => $application
        ]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|string|email|max:255',
        ]);

        $application = Application::where('email', $request->get('email'))->first();
        if($application == NULL)
            $application = Application::create($request->all());
        else
            $application->update($request->except('email'));
        return redirect()->route('application.index')->cookie('email', $request->get('email'), 1);
    }

    public function update(Request $request, Application $application)
    {
        $application->update($request->except('email'));
        return redirect()->route('application.index');
    }
}
