@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <p>
            @if(count($application) == 0)
                У вас нет еще ни одной заявки
            @else
                Ваша заявка:
            @endif
            </p>
            <form class="form-horizontal" action="@if(count($application) == 0){{route('application.store')}}"@else {{route('application.update', $application->id)}}"@endif method="post">
                @if(count($application) > 0)
                    {{ method_field('PUT') }}
                    <input type="hidden" name="id" value="{{ $application->id}}">
                    <p>Номер заявки: {{ $application->id }}</p>
                    <p>Дата заявки: {{ $application->updated_at }}</p>
                @endif
                {{ csrf_field() }}
                <label for="">E-mail</label>
                <input type="email" class="form-control" name="email" value="{{ $application->email or "" }}" required @if(count($application) > 0)readonly @endif>

                <label for="">Текст заявки</label>
                <textarea name="text" class="form-control">{{ $application->text or "" }}</textarea>

                <hr>
                <input type="submit" class="btn btn-primary" value="Сохранить">
            </form>
        </div>
    </div>
</div>
@endsection